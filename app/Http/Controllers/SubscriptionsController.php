<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriptions;
use App\Http\Controllers\Controller;


class SubscriptionsController extends Controller
{
    public function store(Request $request)
    {
        $sub_ver = Subscriptions::where('email', '=', $request->email)->get();
        if (count($sub_ver) == 0) {
            $sub = new Subscriptions;
            $sub->email = $request->email;
            $sub->save();
            return response()->json(json_encode(["message" => "gracias por subscribirse"]), 200);
        }
        return response()->json(json_encode(["message" => "El email ya esta registrado"]), 500);        
    }

}
