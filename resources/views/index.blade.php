<html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" href="images/logo.ico">
        <title>The Best | Futbol Barber</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/jquery.toast.min.css') }}">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row header">
                <div class="col-sm-2 text-center logo">
                    <a href="/">
                        <img class="img-fluid" src="{{ URL::asset('images/logo.png') }}" draggable="false">
                    </a>
                </div>
                <div class="col-sm-4 text-logo">
                    <a href="/" draggable="false">
                        <h2 draggable="false">FUTBOL BARBER</h2>
                    </a>
                </div>
                <div class="col-sm-6 text-right">
                    <span>
                        <img draggable="false" src="{{ URL::asset('images/right-imageblack.png') }}">
                    </span>
                    <img class="img-fluid trama" draggable="false" src="{{ URL::asset('images/trama.png') }}">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 content">
                    <div class="row">
                        <div class="col-sm-2 left-bar"></div>
                        <div class="col-sm-8 mid">
                            <div class="top-bg"></div>
                            <div class="top-bg-left"></div>
                            <div class="row">
                                <div class="col-sm-6 logo-centered">
                                    <img src="{{ URL::asset('images/logo-cont.png') }}" draggable="false" class="logo-cont">
                                </div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-4 right-text">
                                    <div class="pasion">
                                        <img src="{{ URL::asset('images/top-right.png') }}" draggable="false">
                                        <p>Tú pasión</p>
                                        <p>con <span>ESTILO</span></p>
                                    </div>
                                </div>
                            </div>
                            <img src="{{ URL::asset('images/bottom-right.png') }}" draggable="false" class="logo-bottom-right">
                            <div class="bottom-content">
                                <h3>
                                    Subscríbete para estar siempre al día con nuestra novedades y promociones
                                </h3>
                                <form id="form-submit">
                                    <input class="form-input" placeholder="E-MAIL" id="email-form">
                                    @csrf
                                    <button class="form-button">ENVIAR</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-2 right-bar"></div>
                    </div>
                </div>
            </div>
            <div class="row footer">
                <div class="col-sm-4 footer-text">
                    <h4>
                        Calle Dos de Mayo 565, Miraflores, Lima - Perú
                    </h4>
                </div>
                <div class="col-sm-7 footer-text footer-slide">
                    <h5 class="slide-text">Horarios de atención - Lunes a Sábado 11:00am a 9:00pm / Domingos 10:00am - 5:00pm</h5>
                </div>
                <div class="col-sm-1 social-footer">
                    <a target="_blank" href="https://www.facebook.com/TheBestFutbolBarber/">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a target="_blank" href="https://www.instagram.com/thebest.peru/">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="{{ URL::asset('js/jquery.toast.min.js') }}"></script>


        <script>
            $(document).ready(function(){
                var marquee = $('.footer-slide');
                marquee.each(function() {
                    var mar = $(this),indent = mar.width();
                    mar.marquee = function() {
                        indent--;
                        mar.css('text-indent',indent);
                        if (indent < -1 * mar.children('.slide-text').width()) {
                            indent = mar.width();
                        }
                    };
                    mar.data('interval',setInterval(mar.marquee,1000/60));
                });

                $('#form-submit').submit(function(e) {
                    console.log('submit');
                    e.preventDefault();
                    var email = $('#email-form').val();
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    var token = $('input[name=_token]').val();
                    if (regex.test(email)) {
                        
                        var fdata = {'email': email, '_token': token};
                        $.ajax({
                            type: "POST",
                            url: 'subscribe',
                            data: fdata,
                            success: function(data) {
                                $.toast({
                                    heading: 'Enhorabuena',
                                    text: 'Gracias por subscribirse',
                                    position: 'bottom-center',
                                    showHideTransition: 'slide',
                                    icon: 'success'
                                });
                            },
                            error: function(data) {
                                $.toast({
                                    heading: 'Error',
                                    icon: 'warning',
                                    text: 'El email ya esta registrado',
                                    position: 'bottom-center',
                                    showHideTransition: 'slide'
                                });
                            }
                        });

                    } else {
                        $.toast({
                            heading: 'Warning',
                            icon: 'warning',
                            text: 'Debe ingresar un email valido',
                            position: 'bottom-center',
                            showHideTransition: 'slide'
                        });
                    }
                    
                    $('#email-form').val('');
                });
            });

        </script>
    </body>
</html>